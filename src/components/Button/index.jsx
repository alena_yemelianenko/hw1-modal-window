import { Component } from "react";
import styles from './button.module.scss'


export class Button extends Component {
    
    render () {
        return (
            <button className={`${styles.btn} ${this.props.backgroundColor === 'red' ? styles.btnDanger : "" }`} onClick={this.props.onClick} >{this.props.text}</button>
        )
    }
}