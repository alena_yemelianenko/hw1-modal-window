import { Component } from 'react';


import styles from './modal.module.scss'

export class Modal extends Component {

  handleClose = (event) => {
    if (event.target === this.modalRef) {
      this.props.onClose();
    }
  };

  render() {
        return (
        <div className={styles.modalWrap} modalid={this.props.id} onClick={this.handleClose} ref={(node) => (this.modalRef = node)}>
            <div className={styles.modal}>

            <div className={styles.modalHeader}>
                <h2>{this.props.header}</h2>
                { this.props.closeButton ? ( <div className={styles.modalClose} onClick={this.props.onClose}></div>) : null}
            </div>

            <div className={styles.modalText}>{this.props.text}</div>
            <div className={styles.modalActions}>{this.props.actions}</div>
            </div>
        </div>
      );
    }
  
}

