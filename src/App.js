// import logo from './logo.svg';
import { Component } from "react";
import './App.css';
import { Button } from './components/Button';

import { Modal } from './components/Modal';

const modalWindowDeclarations  = [
  {
    id: 'modalID1',
    header: "title for modal 1",
    text: 'description for modal 1',
    closeButton: true,
    actions:  ( 
        <>
          <Button backgroundColor='red' text="Ok" />
          <Button backgroundColor='red' text="Cancel"  />
        </>
    )
    
  },
  {
    id: 'modalID2',
    header: "Do you want to delete this file?",
    text: "Once you delete this file, it won't possible to undo this action. Are you sure you want to delete it?",
    closeButton: true,
    actions:  ( 
      <>
        <Button backgroundColor='red' text="Ok" />
        <Button backgroundColor='red' text="Cancel"  />
      </>
  )
  }];


class App extends Component {
  constructor(props) {
    super(props)
    this.state = { 
      showModal: false,
    } }


  toShowModal = (id) => {
    const modalToOpen =  modalWindowDeclarations.find(modal => modal.id === id);

    this.setState({
       showModal: true,
       modalToRender: modalToOpen
       });
  };
  toCloseModal = () => {
    this.setState({ showModal: false });
  };


  render() {
    return (
      <div >

          <Button
            backgroundColor= 'green'
            text="Open first modal"
            onClick={() => this.toShowModal('modalID1')}

          />

          <Button
            text="Open second modal"
            backgroundColor='red'
            onClick={() => this.toShowModal('modalID2')}

          />

          {this.state.showModal && (
            <Modal {...this.state.modalToRender } onClose={this.toCloseModal}/>
          )}
      </div>
    );
  }
}

export default App;